# syntax = docker/dockerfile:1
FROM python:3-slim-bullseye

ARG RUNTIME_DEPS=" \
  curl \
  bash \
  xz-utils \
  bzip2 \
  default-mysql-client \
  gosu \
  s3cmd \
"

ARG GOCRONVER=v0.0.9
ARG TARGETOS=linux
ARG TARGETARCH=amd64

ARG DB_BACKUP_HEALTHCHECK_PORT=8080

ENV DB_BACKUP_KEEP_DAYS=7 \
  DB_BACKUP_KEEP_WEEKS=4 \
  DB_BACKUP_KEEP_MONTHS=6 \
  DB_BACKUP_SCHEDULE="00    03       *       *       *       " \
  DB_PORT=3306 \
  DB_BACKUP_DIR=/data \
  DB_BACKUP_SUFFIX=.sql.xz \
  DB_BACKUP_HEALTHCHECK_PORT="${DB_BACKUP_HEALTHCHECK_PORT}"

RUN set -x && \
  mkdir ${DB_BACKUP_DIR} && \
  chown -Rv 1001:1001 ${DB_BACKUP_DIR} && \
  apt-get update && \
  apt-get autoremove -y && \
  apt-get install -y --no-install-recommends ${RUNTIME_DEPS} && \
  rm -rf /usr/share/man && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* && \
  curl -L https://github.com/prodrigestivill/go-cron/releases/download/${GOCRONVER}/go-cron-${TARGETOS}-${TARGETARCH}-static.gz | zcat > /usr/local/bin/go-cron && \
  chmod a+x /usr/local/bin/go-cron

USER 1001:1001

COPY backup_mysql.sh docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["start"]

HEALTHCHECK --interval=1m --timeout=10s \
  CMD /docker-entrypoint.sh healthcheck

