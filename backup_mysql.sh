#!/bin/bash

mkdir -p "${DB_BACKUP_DIR}/daily/" "${DB_BACKUP_DIR}/weekly/" "${DB_BACKUP_DIR}/monthly/"

function databases_to_backup(){
	DB_NAMES=$(
		if [ ! "${DB_NAMES}" ] ; then
			mysql  --port="${DB_PORT}" -u "${DB_USER}" -p"${DB_PASSWORD}" --host="${DB_HOST}" \
				--skip-column-names \
				-e "SELECT GROUP_CONCAT(schema_name SEPARATOR ' ') FROM information_schema.schemata WHERE schema_name NOT IN ('mysql','performance_schema','information_schema');"
		else
			echo "${DB_NAMES}"
		fi
	)
	for db_name in ${DB_NAMES} ; do
		for db_exclude in ${DB_EXCLUDE} ; do
			if [ "${db_name}" = "${db_exclude}"  ] ; then
				continue 2
			fi
		done
		echo "${db_name}"
	done
}

function dump_compress(){
	xzcat -z ${DB_COMPRESS_EXTRA_ARGS}
}

function database_dump(){
	echo "Dumping ${DB_USER}@${DB_HOST}:${DB_PORT}/${1} ..." >&2
	mysqldump ${DB_DUMP_EXTRA_ARGS} --host="${DB_HOST}" --port="${DB_PORT}" -u "${DB_USER}" -p"${DB_PASSWORD}" "${1}"
}

function make_backup(){
	echo "Create backup of ${DB_HOST}:${DB_PORT}..."
	KEEP_DAYS=${DB_BACKUP_KEEP_DAYS}
	KEEP_WEEKS=`expr $(((${DB_BACKUP_KEEP_WEEKS}*7)+1))`
	KEEP_MONTHS=`expr $(((${DB_BACKUP_KEEP_MONTHS}*31)+1))`

	DB_FOR_BACKUP=$( databases_to_backup )

	if [ "${DB_SPLIT}" = "true" ] ; then
		for db_name in ${DB_FOR_BACKUP} ; do
			#Initialize filename vers
			PREFIX="${db_name}-${DB_HOST}"
			[[ -n "${DB_BACKUP_PREFIX}" ]] && PREFIX="${DB_BACKUP_PREFIX}"

			DAILY_DATA_FILE="${DB_BACKUP_DIR}/daily/${PREFIX}-`date +%Y-%m-%d-%H:%M:%S`${DB_BACKUP_SUFFIX}"
			WEEKLY_DATA_FILE="${DB_BACKUP_DIR}/weekly/${PREFIX}-`date +%G-%V`${DB_BACKUP_SUFFIX}"
			MONTHLY_DATA_FILE="${DB_BACKUP_DIR}/monthly/${PREFIX}-`date +%Y-%m`${DB_BACKUP_SUFFIX}"

			database_dump "${db_name}" | dump_compress > "${DAILY_DATA_FILE}"

			if [ ! -f "${WEEKLY_DATA_FILE}" ] ; then
				echo "Hard copy data to weekly: ${WEEKLY_DATA_FILE}"
				ln -vf "${DAILY_DATA_FILE}" "${WEEKLY_DATA_FILE}"
			fi
			if [ ! -f "${MONTHLY_DATA_FILE}" ] ; then
				echo "Hard copy data to monthly files: ${MONTHLY_DATA_FILE}"
				ln -vf "${DAILY_DATA_FILE}" "${MONTHLY_DATA_FILE}"
			fi

			echo "Cleaning older backups of ${db_name}..."
			find "${DB_BACKUP_DIR}/daily" -maxdepth 1 -mtime +${KEEP_DAYS} -name "${PREFIX}-*${DB_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'
			find "${DB_BACKUP_DIR}/weekly" -maxdepth 1 -mtime +${KEEP_WEEKS} -name "${PREFIX}-*${DB_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'
			find "${DB_BACKUP_DIR}/monthly" -maxdepth 1 -mtime +${KEEP_MONTHS} -name "${PREFIX}-*${DB_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'
		done
		#mysqldump --no-tablespaces --routines --add-drop-table --disable-keys --extended-insert --host="${DB_HOST}" --port="${DB_PORT}" -u "${DB_USER}" -p"${DB_PASSWORD}" "${DB_NAME}" | xzcat -z > "${DAILY_DATA_FILE}"
	else
		#Initialize filename vers
		db_name="all"
		PREFIX="${db_name}-${DB_HOST}"
		[[ -n "${DB_BACKUP_PREFIX}" ]] && PREFIX="${DB_BACKUP_PREFIX}"

		DAILY_DATA_FILE="${DB_BACKUP_DIR}/daily/${PREFIX}-`date +%Y-%m-%d-%H:%M:%S`${DB_BACKUP_SUFFIX}"
		WEEKLY_DATA_FILE="${DB_BACKUP_DIR}/weekly/${PREFIX}-`date +%G-%V`${DB_BACKUP_SUFFIX}"
		MONTHLY_DATA_FILE="${DB_BACKUP_DIR}/monthly/${PREFIX}-`date +%Y-%m`${DB_BACKUP_SUFFIX}"

		for db in ${DB_FOR_BACKUP} ; do
			database_dump "${db}"
		done | dump_compress > "${DAILY_DATA_FILE}"

		if [ ! -f "${WEEKLY_DATA_FILE}" ] ; then
			echo "Hard copy data to weekly: ${WEEKLY_DATA_FILE}" >&2
			ln -vf "${DAILY_DATA_FILE}" "${WEEKLY_DATA_FILE}"
		fi
		if [ ! -f "${MONTHLY_DATA_FILE}" ] ; then
			echo "Hard copy data to monthly files: ${MONTHLY_DATA_FILE}" >&2
			ln -vf "${DAILY_DATA_FILE}" "${MONTHLY_DATA_FILE}"
		fi

		echo "Cleaning older backups of ${db_name}..."
		find "${DB_BACKUP_DIR}/daily" -maxdepth 1 -mtime +${KEEP_DAYS} -name "${PREFIX}-*${DB_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'
		find "${DB_BACKUP_DIR}/weekly" -maxdepth 1 -mtime +${KEEP_WEEKS} -name "${PREFIX}-*${DB_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'
		find "${DB_BACKUP_DIR}/monthly" -maxdepth 1 -mtime +${KEEP_MONTHS} -name "${PREFIX}-*${DB_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'

		#mysqldump --no-tablespaces --routines --add-drop-table --disable-keys --extended-insert --host="${DB_HOST}" --port="${DB_PORT}" -u "${DB_USER}" -p"${DB_PASSWORD}" --databases ${DATABASES} | xzcat -z > "${DAILY_DATA_FILE}"
	fi
}

function s3_upload(){
	(
		cd "${DB_BACKUP_DIR}"
		S3_ARGS=""

		[[ -n "${S3_ENDPOINT}" ]] && S3_ARGS="${S3_ARGS} --host ${S3_ENDPOINT} --host-bucket ${S3_ENDPOINT}"
		[[ -n "${S3_REGION}" ]] && S3_ARGS="${S3_ARGS} --region=${S3_REGION}"
		if [ "${S3_READONLY}" = true ] ; then
			S3_ARGS="${S3_ARGS} --skip-existing"
		else
			S3_ARGS="${S3_ARGS} --delete-removed"
		fi

		if [ "${S3_ENDPOINT}" ] ; then
			s3cmd --access_key "${S3_ACCESS_KEY}" \
				--secret_key "${S3_SECRET_KEY}" \
				${S3_ARGS} \
				sync "." "s3://${S3_BUCKET}${S3_BUCKET_DIR}"
		fi
	)
}

make_backup

if [ "${S3_ENABLE}" = "true" ] ; then
	s3_upload
fi

