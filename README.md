![Docker Pulls](https://img.shields.io/docker/pulls/psychomantys/mysql-backup)

# mysql-backup

Backup MySQL periodically, based on idea of [prodrigestivill/docker-postgres-backup-local](https://hub.docker.com/r/prodrigestivill/docker-postgres-backup-local/).

Supports the following Docker architectures: `linux/amd64`, `linux/arm64`, `linux/arm/v7`.

## Usage

Bind the mysql data directory to the container, set the envs and bind the backup directory.

Docker:

```sh
docker run -v ${DB_BACKUP_OUT}:/data -e DB_BACKUP_KEEP_DAYS=7 -e DB_BACKUP_KEEP_WEEKS=4 -e DB_BACKUP_KEEP_MONTHS=6 -e "DB_USER=${DB_USER}" -e "DB_PASSWORD=${DB_PASSWORD}" -e DB_HOST=${DB_HOST} -e DB_PORT=${DB_PORT} psychomantys/mysql-backup
```

Docker Compose:

```yaml
version: '3.8'
services:
  app-db:
    image: mariadb
    environment:
      MYSQL_RANDOM_ROOT_PASSWORD: "yes"
      MYSQL_DATABASE: ${DB_NAME}
      MYSQL_USER: ${DB_USER}
      MYSQL_PASSWORD: ${DB_PASSWORD}
      MYSQL_TCP_PORT: ${DB_PORT}
    volumes:
      - app_db_data:/var/lib/mysql

  app-backup-db:
    image: psychomantys/mysql-backup:latest
    environment:
      DB_BACKUP_KEEP_DAYS: 7
      DB_BACKUP_KEEP_WEEKS: 4
      DB_BACKUP_KEEP_MONTHS: 12
      DB_BACKUP_SCHEDULE: "00    03       *       *       *       "
      DB_NAMES: ${DB_NAME}
      DB_USER: ${DB_USER}
      DB_PASSWORD: ${DB_PASSWORD}
      DB_HOST: ${DB_HOST}
      DB_PORT: ${DB_PORT}
    volumes:
      - type: bind
        source: /home/app/backup/mariadb/
        target: /data

volumes:
  app_db_data:
```

### Environment Variables

| env variable | Description |
|--|--|
| DB_EXCLUDE | List of database names splited by whitespace to exclude from backup |
| DB_NAMES | Space separated list of tables. If empty, backup all database. Default is empty. |
| DB_USER | User to bind and make the backup |
| DB_PASSWORD | Password for bind user |
| DB_HOST | Host of mysql server |
| DB_PORT | Port of running mysql server. Default to `3306` |
| DB_BACKUP_DIR | Directory to save the backup at. Defaults to `/data`. |
| DB_BACKUP_SUFFIX | Filename suffix to save the backup. Defaults to `.ldif.xz`. |
| DB_BACKUP_KEEP_DAYS | Number of daily backups to keep before removal. Defaults to `7`. |
| DB_BACKUP_KEEP_WEEKS | Number of weekly backups to keep before removal. Defaults to `4`. |
| DB_BACKUP_KEEP_MONTHS | Number of monthly backups to keep before removal. Defaults to `6`. |
| DB_BACKUP_SCHEDULE | Cron like string to set daily execution of backup script. Defaults to `00 03 * * *`. |
| DB_SPLIT | If set to `true`, split dump in individual files |
| DB_DUMP_EXTRA_ARGS | Extra args to compress |
| DB_COMPRESS_EXTRA_ARGS | Extra args to dump database |
| DB_BACKUP_PREFIX | Prefix for backup file. Default is `"${db_name}-${DB_HOST}"` |

### S3/Bucket Variables

| env variable | Description |
|--|--|
| S3_ENABLE | Enable S3 backup |
| S3_ACCESS_KEY | Access key to access bucket |
| S3_SECRET_KEY | Secret Key of bucket |
| S3_ENDPOINT | S3 Endpoint, no defaults, can be a minio host |
| S3_BUCKET | Bucket name |
| S3_BUCKET_DIR | Bucket Directory to upload backups |
| S3_REGION | Region of bucket, no defaults for now |
| S3_READONLY | Enable not delete anything on S3, for delete files in bucket policy |

### Manual Backups

By default, the backup is make daily at `03:00`. But, you can force a backup running the script `/backup_mysql.sh`.

To force a backup now with a container created on first section, you can make:

```sh
docker exec -it "${CONTAINER}" /backup_mysql.sh
```

### Automatic Periodic Backups

You can change the `DB_BACKUP_SCHEDULE` environment variable in `-e DB_BACKUP_SCHEDULE=` to alter the default frequency. Default is `00 03 * * *`.

More information about the scheduling can be found [here](cron.guru).

Folders `daily`, `weekly` and `monthly` are created and populated using hard links to save disk space.

## Restore examples

Some examples to restore/apply the backups.

### Restore to a remote server

Configure the container like in compose example. Replace the `${BACKUP_FILENAME}` and `${CONTAINER}`, from the following command:

```sh
docker exec -ti "${CONTAINER}" db_restore "${BACKUP_FILENAME}"
```

For example:

```sh
docker exec -ti mysql-backup_app-backup-db_1 db_restore "/data/daily/opa-2020-10-31-02:20:00.sql.xz"
```

