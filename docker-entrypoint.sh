#!/bin/bash

set -e

do_gosu(){
	user="$1"
	shift 1

	is_exec="false"
	if [ "$1" = "exec" ] ; then
		is_exec="true"
		shift 1
	fi

	if [ "$(id -u)" = "0" ] ; then
		if [ "${is_exec}" = "true" ] ; then
			exec gosu "${user}" "$@"
		else
			gosu "${user}" "$@"
		fi
	else
		if [ "${is_exec}" = "true" ] ; then
			exec "$@"
		else
			eval "$@"
		fi
	fi
}

parse_env(){
	if [ -r "$1" ] ; then
		while IFS="=" read key value  ; do
			export "${key}=${value}"
		done<<<"$( egrep '^[^#]+=.*' "$1" )"
	fi
}

parse_env '/env.sh'
parse_env '/run/secrets/env.sh'

if [ "start" = "$1" ]; then
	#echo "${DB_BACKUP_SCHEDULE} /backup_mysql.sh" | crontab -
	#exec crond -l 2 -f
	do_gosu 1001:1001 exec /usr/local/bin/go-cron -s "${DB_BACKUP_SCHEDULE}" -p "${DB_BACKUP_HEALTHCHECK_PORT}" -- /backup_mysql.sh
elif [ "db_restore" = "$1" ]; then
	echo "Restoring database...."
	do_gosu 1001:1001 mysql --host="${DB_HOST}" --port="${DB_PORT}" -u "${DB_USER}" -p"${DB_PASSWORD}" < "${2}"
	if [ "$?" = "0" ] ; then
		echo "Restore end"
		exit 0
	else
		echo "Restore ERROR!"
		exit 1
	fi
elif [ "healthcheck" == "$1" ]; then
	do_gosu 1001:1001 curl -f "http://127.0.0.1:${DB_BACKUP_HEALTHCHECK_PORT}/" || exit 1
	exit 0
fi

exec "$@"

